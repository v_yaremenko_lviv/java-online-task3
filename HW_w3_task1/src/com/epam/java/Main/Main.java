package com.epam.java.Main;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        System.out.println("Welcome to our zoo!!" + "\n"
                + "Please enter what animal you want to seeing:" + "\n"
                + "№1 Fish, "
                + "№2 Eagle, "+ "\n");

        int setAnimal = scanner.nextInt();

         if (setAnimal == 1) {
            Fish myFish = new Fish("Flaunder");
            myFish.eat(); myFish.swim();myFish.swim();

        }else if (setAnimal == 2) {
            Eagle myEagle = new Eagle("Rikko");
            myEagle.eat();myEagle.fly();myEagle.sleep();

        } else {System.out.println("\n" +
                "Sorry we do not have such animals");}
    }
}