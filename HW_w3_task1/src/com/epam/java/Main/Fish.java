package com.epam.java.Main;

public class Fish extends Animal {
    public Fish(String name) {
        this.name = name;
    }

    @Override
    public void sleep() {
        System.out.println(String.format("Fish %s is a sleep on right side", this.name));
    }
    @Override
    public void eat() {
        System.out.println(String.format("Fish %s eats plankton, and non humble children", this.name));
    }

    public void swim(){
        System.out.println(String.format("Fish %s swim in water", this.name));
    }
}