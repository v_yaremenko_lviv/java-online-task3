package com.epam.java.Main;

public class Bird extends Animal {
    Bird(String name) {
        this.name = name;
    }

    @Override
    protected void sleep() {
        System.out.println(String.format("Bird %s sleeps", this.name));
    }
    @Override
    protected void eat() {
        System.out.println(String.format("Bird %s eats anything", this.name));
    }
    protected void fly(){
        System.out.println(String.format("Bird %s flies", this.name));
    }
}
