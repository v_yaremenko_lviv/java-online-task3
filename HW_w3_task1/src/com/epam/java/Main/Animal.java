package com.epam.java.Main;

public abstract class Animal {
    void sleep() {
        System.out.println("Animal is asleep");
    }

    void eat(){
        System.out.println("Animal eats anything");
    }

    protected String name;
    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

}