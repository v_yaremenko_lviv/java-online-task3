package com.epam.java.main;

public class Circle extends Shape{
    private  int radius;
    Circle(String color, int radius) {
        super(color);
        this.radius = radius;
    }
    public double getArea(){
        return Math.pow(this.radius, 2) * Math.PI;
    }
}
