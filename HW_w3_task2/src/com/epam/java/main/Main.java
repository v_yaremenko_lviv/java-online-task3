package com.epam.java.main;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        System.out.println(
                "enter the length of the triangle first edge :" + "\n");
        int s =  scanner.nextInt();

        System.out.println(
                "enter the length of the triangle second edge :" + "\n");
        int l =  scanner.nextInt();

        System.out.println(
                "enter the length of the triangle third edge :" + "\n");
        int m =  scanner.nextInt();

        //multicolored things ))
        Rectangle redRect = new Rectangle("Red", 10, 15);
        Rectangle yellowRect = new Rectangle("Yellow", 4, 11);
        Rectangle blueRect = new Rectangle("Blue", 2, 10);

        //Perimeter of triangle
        Triangle orangeTriangle = new Triangle("Orange");
        orangeTriangle.setK(
                new int[]{s,l,m}
                );

        orangeTriangle.setY(
                new int[]{s,l,m}
                );
        System.out.println("The orange triangle perimeter is : " + orangeTriangle.getPerimeter());

        //green circle area
        Circle greenCircle = new Circle("Green", 10);
        System.out.println("The green circle area is : " + greenCircle.getArea());
    }
}
