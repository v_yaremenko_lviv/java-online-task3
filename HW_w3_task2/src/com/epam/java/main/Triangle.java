package com.epam.java.main;

public class Triangle extends Shape {
    private int[] k = new int[3];
    private int[] y = new int[3];

    Triangle(String color) {
        super(color);
    }
    public void setK(int[] k) {
        this.k = k;
    }
    public void setY(int[] y) {
        this.y = y;
    }
    private double getLength(int x1, int y1, int x2, int y2){
        return Math.sqrt(Math.pow(x1 - x2, 2) + Math.pow(y1 - y2, 2));
    }
    public  double getPerimeter(){
        double a = getLength(k[0], y[0], k[1], y[1]);
        double b = getLength(k[0], y[0], k[2], y[2]);
        double c = getLength(k[2], y[2], k[1], y[1]);
        return a + b + c;
    }
}
