package com.epam.java.main;

public class Rectangle extends Shape{
    private int height, width;

    Rectangle(String color, int height, int width) {
        super(color);
        this.height = height;
        this.width = width;
    }
    public int getHeight() {
        return height;
    }
    public int getWidth() {
        return width;
    }
    public void setHeight(int height) {
        this.height = height;
    }
    public void setWidth(int width) {
        this.width = width;
    }
    public int getArea(){
        return this.height * this.width;
    }
}
